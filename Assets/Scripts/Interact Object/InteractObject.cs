using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractObject : MonoBehaviour
{
    public GameObject tarGet;
    public GameObject objectCanvas;
    public GameObject Way;
    public bool isPlayerHere;
    public bool isSucceed = false;
    public bool isInteract = false;
    private bool isInteractChack = false;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && isPlayerHere)
        {
            if (isInteract == true)
            {
                if (isInteractChack == false)
                {
                    if (isSucceed == false)
                    {
                        tarGet.SetActive(false);
                        isSucceed = true;
                        isInteractChack = true;
                    }
                    else if (isSucceed == true)
                    {
                        tarGet.SetActive(true);
                        isSucceed = false;
                        isInteractChack = true;
                    }
                }
                else if (isInteract == false)
                {
                    if (isSucceed == false)
                    {
                        tarGet.SetActive(false);
                        isSucceed = true;

                    }
                    else if (isSucceed == true)
                    {
                        tarGet.SetActive(true);
                        isSucceed = false;
                    }
                }
            }
            Destroy(this.gameObject);
            Way.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayerHere = true;
            objectCanvas.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayerHere = false;
            objectCanvas.SetActive(false);
        }
    }
}
