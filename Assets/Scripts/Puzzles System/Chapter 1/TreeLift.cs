using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeLift : MonoBehaviour
{
    public GameObject seedLift;
    public bool isHaveSeed;
    public bool isHaveLight;
    public bool isHaveWater;
    public bool isPlayerInArea;

    void Start()
    {
        
    }

    
    void Update()
    {
        if (isPlayerInArea)
        {
            seedLift.SetActive(true);  
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            isPlayerInArea = true;
        }
    }

}
