using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedLift : MonoBehaviour
{
    public Animator Animator;

    void Start()
    {
        Animator = GetComponent<Animator>();
    }
    
    void Update()
    {
        
    }

    public void Glowing()
    {
        Animator.SetBool("IsGlowing", true);
    }
}
