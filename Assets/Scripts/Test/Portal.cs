using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public bool isWarp;
    public float distance;

    [SerializeField] Transform playerPosition;
    
    void Start()
    {
        if(isWarp == false)
        {
            playerPosition = GameObject.FindGameObjectWithTag("Next Position").GetComponent<Transform>();
        }
        else
        {
            playerPosition = GameObject.FindGameObjectWithTag("Back Position").GetComponent<Transform>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(Vector3.Distance(transform.position, other.transform.position) < distance)
        {
            other.transform.position = new Vector3(playerPosition.position.x, 0f , playerPosition.position.y);
        }
    }
}
