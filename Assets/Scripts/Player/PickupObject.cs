using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObject : MonoBehaviour
{
    public float pickupDistance;
    public float forceMulti;
    public bool isPickup;
    public bool canThrow;

    private Transform pickUpPoint;
    private Transform player;
    private Rigidbody objectRig;

    void Start()
    {
        objectRig = GetComponent<Rigidbody>();
        player = GameObject.Find("Player").transform;
        pickUpPoint = GameObject.Find("PickUpPoint").transform;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.E) && isPickup == true && canThrow)
        {
            forceMulti += 300 * Time.deltaTime;
        }

        pickupDistance = Vector3.Distance(player.position, transform.position);

        if(pickupDistance <= 2)
        {
            if (Input.GetKey(KeyCode.E) && isPickup == false && pickUpPoint.childCount < 1)
            {
                GetComponent<Rigidbody>().freezeRotation = true;
                this.transform.position = pickUpPoint.position;
                this.transform.parent = GameObject.Find("PickUpPoint").transform;

                isPickup = true;
                forceMulti = 0;
            }
        }

        if(Input.GetKeyUp(KeyCode.E) && isPickup == true)
        {
            canThrow = true;

            if(forceMulti > 5)
            {
                objectRig.AddForce(player.transform.forward * forceMulti);
                this.transform.parent = null;
                GetComponent<Rigidbody>().freezeRotation = false;
                isPickup = false;
                forceMulti = 5;
                canThrow = false;
            }

            forceMulti = 0;
        }
    }
    /*public void CollectItem(testDelegate2 item)
    {
        //item.trasform.position = hand.transform.position
        //item.transform.setparent(hand.trasnform)
        float excessWeight = item.weight - weightThreshold;
        if (excessWeight > 0)
        {
            //1 kg = 10%
            //kg = 50%
            weightSpeed -= excessWeight / 10f;
        }
        currentItemInHand = item;

    }
    public void Throw()
    {
        currentItemInHand.transform.parent = null;
        //add force
        currentItemInHand = null;
        weightSpeed = 1;
    }*/
}
