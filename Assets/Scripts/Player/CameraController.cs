using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float smoothing;
    private Vector3 offSet;
    
    void Start()
    {
        offSet = transform.position - target.position;
    }

    
    void Update()
    {
        Vector3 targetCamera = target.position + offSet;
        transform.position = Vector3.Lerp(transform.position, targetCamera, smoothing * Time.deltaTime);
    }
}
