using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMechanic : MonoBehaviour
{
    public int playerHp;

    void Start()
    {
        
    }

    public void TakeDamage(int damage)
    {
        playerHp = playerHp - damage;
        if (playerHp <= 1)
        {

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeathZone")
        {
            TakeDamage(1);
            Debug.Log("Dead");
        }
    }

    void Update()
    {

    }
}
