using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerControllerRigidbody : MonoBehaviour
{
    #region Movement
    [SerializeField] private Animator playerAnimator;
    [SerializeField] private Rigidbody playerRigidbody;
    [SerializeField] private Transform pickupPoint;
    [SerializeField] private Collider playerCollider;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float moveSpeedOriginal;
    [SerializeField] private float jumpForce;
    [SerializeField] private float jumpForceOriginal;
    [SerializeField] private float jumpCountCurrent;
    [SerializeField] private float jumpCountMax;
    //[SerializeField] private int extraJumpsValue;
    [SerializeField] private float climbSpeed;
    [SerializeField] private float slidingSpeed;
    [SerializeField] private float pickupRange;

    private Vector3 movement;
    //private int extraJumps;
    #endregion

    #region Checking
    [SerializeField] private bool isMoving = false;
    [SerializeField] private bool isSneaking = false;
    [SerializeField] private bool isGrounded = false;
    [SerializeField] private bool isJumping = false;
    [SerializeField] private bool isClimbing = false;
    [SerializeField] private bool isSliding = false;
    [SerializeField] private bool isHit = false;
    [SerializeField] private bool isPickUp = false;
    [SerializeField] private bool isPushing = false;
    #endregion

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        playerCollider = GetComponent<Collider>();
        //extraJumps = extraJumpsValue;
        playerAnimator = GetComponent<Animator>();
    }

    void Update()
    {
        float moveX = Input.GetAxis("Horizontal");
        float moveZ = Input.GetAxis("Vertical");
        movement = new Vector3(moveX, 0, moveZ);
        movement.Normalize();

        Move(movement);

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Sneak();
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            SneakCancel();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        if(isClimbing)
        {
            ClimbingRope();
        }

        if(isSliding)
        {
            Vector3 currentPosition = transform.position;
            currentPosition.y -= slidingSpeed * Time.deltaTime;
            transform.position = currentPosition;
        }
    }

    public void Teleport(Vector3 position)
    {
        transform.position = position;
        Physics.SyncTransforms();
    }

    #region Movement
    private void Move(Vector3 moveDirection)
    {
        playerRigidbody.AddForce(moveDirection * moveSpeed);

        if (moveDirection != Vector3.zero)
        {
            playerAnimator.SetBool("IsMoving", true);
            isMoving = true;
            Quaternion toRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }
        else
        {
            playerAnimator.SetBool("IsMoving", false);
            isMoving = false;
        }
    }

    private void Sneak()
    {
        if (!isSneaking)
        {
            moveSpeed -= 1.5f;
            isSneaking = true;
            playerAnimator.SetBool("IsSneaking", true);
        }
    }

    private void SneakCancel()
    {
        if (isSneaking)
        {
            moveSpeed += 1.5f;
            isSneaking = false;
            playerAnimator.SetBool("IsSneaking", false);
        }
    }

    private void Jump()
    {
        if (jumpCountCurrent > 0)
        {
            jumpCountCurrent--;
            playerRigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            //playerAnimator.SetBool("IsJumping", true);
        }
    }

    /*private void ExtraJump()
    {
        if (isGrounded == true)
        {
            extraJumps = extraJumpsValue;
        }

        if (Input.GetKeyDown(KeyCode.Space) && extraJumps > 0)
        {
            playerRig.velocity = new Vector3(playerRig.velocity.x, jumpForce, playerRig.velocity.z);
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && extraJumps == 0 && isGrounded == true)
        {
            playerRig.velocity = new Vector3(playerRig.velocity.x, jumpForce, playerRig.velocity.z);
        }
    }*/

    private void ClimbingRope()
    {
        if(Input.GetKeyDown(KeyCode.W))
        {
            playerRigidbody.AddForce(Vector3.up * climbSpeed);
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            playerRigidbody.AddForce(Vector3.down * climbSpeed);
        }

        /*if(Input.GetKey(KeyCode.A))
        {
            playerRig.AddForce(Vector3.left * climbSpeed);
        }

        if(Input.GetKey(KeyCode.D))
        {
            playerRig.AddForce(Vector3.right * climbSpeed);
        }*/
    }

    private void PickUpObject()
    {

    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            jumpCountCurrent = jumpCountMax;
            jumpForce = jumpForceOriginal;
            isGrounded = true;
            isJumping = false;
            //playerAnimator.SetBool("IsJumping", false);
        }

        if (collision.collider.tag == "Edge")
        {
            if (!isGrounded)
            {
                playerRigidbody.useGravity = false;
                jumpForce += 0.5f;
                jumpCountCurrent = 1;
                isClimbing = true;
                isJumping = false;
            }
        }

        if (collision.collider.tag == "WallJump")
        {
            if (!isGrounded)
            {
                playerRigidbody.useGravity = false;
                jumpForce += 0.5f;
                jumpCountCurrent = 1;
                isClimbing = true;
                isSliding = true;
                isJumping = false;
            }
        }

        if (collision.collider.tag == "Rope")
        {
            if (!isGrounded)
            {
                playerRigidbody.useGravity = false;
                jumpCountCurrent = 1;
                isClimbing = true;
                isJumping = false;
            }
        }

        if (collision.collider.tag == "PickUpObject")
        {
            isPickUp = true;
        }

        if (collision.collider.tag == "PushingObject")
        {
            isPushing = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Floor")
        {
            isGrounded = false;
            isJumping = true;
        }

        if (collision.collider.tag == "Edge")
        {
            playerRigidbody.useGravity = true;
            jumpForce -= 0.5f;
            isClimbing = false;
            isJumping = true;
        }

        if (collision.collider.tag == "WallJump")
        {
            playerRigidbody.useGravity = true;
            jumpForce -= 0.5f;
            isClimbing = false;
            isSliding = false;
            isJumping = true;
        }

        if (collision.collider.tag == "Rope")
        {
            playerRigidbody.useGravity = true;
            isClimbing = false;
            isJumping = true;
        }

        if (collision.collider.tag == "PickUpObject")
        {
            isPickUp = false;            
        }

        if (collision.collider.tag == "PushingObject")
        {
            isPushing = false;
        }
    }

    #endregion
}