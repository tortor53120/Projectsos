using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpPosition : MonoBehaviour
{
    public GameObject targetPosition;
    private Rigidbody playerRig;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            if (playerRig == null)
            {
                playerRig = collision.gameObject.GetComponent<Rigidbody>();
            }
            playerRig.Sleep();
            playerRig.velocity = Vector3.zero;
            collision.transform.position = targetPosition.transform.position;
        }
    }
}
