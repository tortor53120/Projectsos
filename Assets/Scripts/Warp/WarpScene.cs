using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WarpScene : MonoBehaviour
{
    public int sceneNext;
    public GameObject Canvas;
    public bool isPlayerinArea;

    void Start()
    {

    }

    void Update()
    {
        if (isPlayerinArea)
        {
            SceneManager.LoadScene(sceneNext);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            isPlayerinArea = true;
            Canvas.SetActive(true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Player")
        {
            isPlayerinArea = false;
            Canvas.SetActive(false);
        }
    }
}